# STARTER

## PACKAGE JSON
* CMD
    ** npm run watch
    ** npm uninstall [package] -D //supp devdependencies

* linter : npm i eslint -D ; eslint --init
    ** airBnB
    ** https://stackoverflow.com/questions/37826449/expected-linebreaks-to-be-lf-but-found-crlf-linebreak-style
    ** https://stackoverflow.com/questions/34174207/how-to-change-indentation-in-visual-studio-code

* jest : jest --init

* webpack : npm install webpack webpack-cli --save-dev
    * npm install --save-dev babel-loader babel-core
        ** Babel
        **npm install --save-dev @babel/preset-env
    
    * npm install webpack-dev-server --save-dev
        ** serveur de développement

    * npm install  lodash -D

    * npm install sass-loader node-sass webpack --save-dev
        ** SCSS cf config

    * npm install --save-dev style-loader css-loader
        ** CSS

    * npm i -D postcss-loader 

    * npm install --save-dev file-loader
        ** Load image / Load Fonts

    * npm install --save-dev csv-loader xml-loader
        ** Import json, csv, xml

    * npm install url-loader file-loader --save-dev
        ** Url loader size and name
    
    * npm i html-webpack-plugin -D
        ** Generating Multiple HTML Files
        ** DOC: https://www.npmjs.com/package/html-webpack-plugin
            *** https://github.com/numical/script-ext-html-webpack-plugin Ajoute async defer etc...
    
    * npm i cross-env
        ** https://www.npmjs.com/package/cross-env
    
    
