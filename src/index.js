import { join } from 'lodash';
import './styles/style.scss';

import data from './data';

function component() {
  const element = document.createElement('div');

  element.innerHTML = join(['Fight Club :', 'The first rule is... Use My Starter ;)'], ' ');
  element.classList.add('hello');

  const btn = document.createElement('button');
  btn.innerHTML = 'Click me and check the console!';
  btn.onclick = data;
  element.appendChild(btn);

  return element;
}

//  let jaimeleHumus = 'jaime le humus !!!!'

document.body.appendChild(component());
